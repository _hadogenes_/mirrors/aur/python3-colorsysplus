#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''colorsysplus''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1637376062)),
  description='''An extension of the standard colorsys module with support for CMYK, terminal colors, ANSI and more.''',
  author='''Xyne''',
  author_email='''gro xunilhcra enyx, backwards''',
  url='''http://xyne.dev/projects/python3-colorsysplus''',
  packages=['''colorsysplus'''],
)
