# 2021-01-02
* Fix pylint warnings.
* Update copyright headers.
* Remove function vector_distance_squared.
* Removed redundant encoding comments.
* Completely changed formatter classes (not backwards-compatible).

The previous formatter classes were hacks that used placeholder fields to insert colors at arbitrary points in the format and template strings. The new classes instead add a custom color specification to the different format specifications as the color is an aspect of each field's formatting.

# 2019-12-29
* Converted to PEP8 formatting and fixed most warnings.
* Added module to support colors in format, percent-style and template strings: `colorsysplus.format`.
* Restructured modules in package: `ColorizedText` -> `colorsysplus.text`, `ColorizedLog` -> `colorsysplus.Log`.

# 2017-11-01
* Added colorlog module with code from pacboy project. The `configure_logging`
  function can be used to colorize the output of the standard `logging` module.

# 2015-12-23
* Merged ANSI functions from XCGF module.
* Reworked ANSI SGR parsing.
* Simplified color projection code for standard 256 terminal colors.
* Changed terminal and ANSI color functions.
* Added rgb_to_ansi and ansi_to_rgb.
* Miscellaneous other changes (sorry).
* Added ctconv.

# 2012-10-22
* Added ColorizedText module.
